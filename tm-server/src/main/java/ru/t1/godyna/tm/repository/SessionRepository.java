package ru.t1.godyna.tm.repository;

import ru.t1.godyna.tm.api.repository.ISessionRepository;
import ru.t1.godyna.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
