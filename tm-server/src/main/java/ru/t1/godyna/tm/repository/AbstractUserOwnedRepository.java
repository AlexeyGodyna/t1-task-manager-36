package ru.t1.godyna.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.IUserOwnedRepository;
import ru.t1.godyna.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void removeAll(@Nullable final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) return null;
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @NotNull
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null) return null;
        return removeOneById(userId, model.getId());
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return (int) records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}
