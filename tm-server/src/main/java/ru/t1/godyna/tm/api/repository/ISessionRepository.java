package ru.t1.godyna.tm.api.repository;

import ru.t1.godyna.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session>{
}
