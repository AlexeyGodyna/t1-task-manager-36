package ru.t1.godyna.tm.api.service;

import ru.t1.godyna.tm.model.Session;

public interface ISessionService extends IUserOwnerService<Session> {
}
