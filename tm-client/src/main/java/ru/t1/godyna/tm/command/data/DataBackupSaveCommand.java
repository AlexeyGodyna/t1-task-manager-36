package ru.t1.godyna.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.domain.DataBackupSaveRequest;
import ru.t1.godyna.tm.enumerated.Role;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";

    @Getter
    @NotNull
    private final String description = "Save backup to file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("Save backup");
        getDomainEndpoint().saveDataBackup(new DataBackupSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }


}
