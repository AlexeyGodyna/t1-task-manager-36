package ru.t1.godyna.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.godyna.tm.dto.response.project.ProjectShowByIndexResponse;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectShowCommand {

    @NotNull
    private final String NAME = "project-show-by-index";

    @NotNull
    private final String DESCRIPTION = "Display project by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken(), index);
        @NotNull final ProjectShowByIndexResponse response = getProjectEndpoint().showProjectByIndex(request);
        showProject(response.getProject());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
