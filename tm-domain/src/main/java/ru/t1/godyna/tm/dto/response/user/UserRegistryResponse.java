package ru.t1.godyna.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.response.AbstractUserResponse;
import ru.t1.godyna.tm.model.User;

@NoArgsConstructor
public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
